var fs = require("fs");

/**
 * Logger class
 * @type {Function}
 */

var Logger = module.exports = function(fileLevel, screenLevel, path){
    this.debugLevelFile = fileLevel;
    this.debugLevelScreen = screenLevel;
    this.logPath = path;
};

/**
 * Logs to screen and to a file
 * @param level log level
 * @param message message to log
 */
Logger.prototype.log = function(level, message) {
    var levels = ["debug", "error", "warn", "info"],
        output,
        now = new Date(),
        tempDate;

    if (typeof message !== 'string') {
        message = JSON.stringify(message);
    }
    output = level + ": " + message;

    if (levels.indexOf(level) >= levels.indexOf(this.debugLevelFile) ) {
        tempDate = now.getMonth()+1 + "/" + now.getDate() + "/" + now.getFullYear() + " " +
                   now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds() + ":" + now.getMilliseconds();

        fs.appendFile(this.logPath, tempDate + " " + output + "\n", function(err){
            if(err) {
                console.log("Error writing to log");
            }
        });
    }

    if (levels.indexOf(level) >= levels.indexOf(this.debugLevelScreen) ) {
        console.log(output);
    }

};
