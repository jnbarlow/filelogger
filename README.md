Filelogger is a logging utility to allow logging to the console AND filesystem with differing log levels for each.

The default log levels are as follows:

* debug
* error
* warn
* info

For example, if you select "warn" for the console logging, everything below warn will be logged to the console (warn, info).
Likewise, if you select "debug", all four levels will be logged.

**Usage**

The Logger class takes three elements:

* fileLevel (debug -> info)
* consoleLevel (debug -> info)
* path (file path for the log)

```javascript

var Logger = require("filelogger"),
    logger = new Logger("error", "info", "myLog.log");

```

To log, simply call logger.log() with the log level and message (can also be an object);

```javascript
logger.log("info", "Foobar!");

logger.log("error", {message: "my error message"});
```

